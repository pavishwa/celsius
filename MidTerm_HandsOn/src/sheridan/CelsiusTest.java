package sheridan;
import static org.junit.Assert.*;

import org.junit.Test;

/*
 * Author: Vishwa Patel
 * SID: 991516280
 * */
public class CelsiusTest {

	@Test
	public void testCelsiusRegular() {
		int result = Celsius.formFahrenheit(32);
		assertTrue("result matches",  result == 0);
	}
	
	@Test
	public void testCelsiusException() {
		int result = Celsius.formFahrenheit(-50);
		assertFalse("The temp provided is not valid", result == -97.2);
	}
	
	@Test
	public void testCelsiusBoundryIn() {
		int result = Celsius.formFahrenheit(37);
		assertFalse("result matches",  result == 7.388);
	}
	
	@Test
	public void testCelsiusBoundryOut() {
		int result = Celsius.formFahrenheit(53);
		assertFalse("The temp provided is not valid", result == 78.67);
	}
	
}
